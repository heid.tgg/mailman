import os


from sqlalchemy import Table, Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
Base = declarative_base()


person_to_conversation_table = Table('person_to_conversation', Base.metadata,
    Column('person.id', Integer, ForeignKey('person.id')),
    Column('conversation.id', Integer, ForeignKey('conversation.id'))
)
mail_to_conversation_table = Table('mail_to_conversation', Base.metadata,
                          Column('mail.id', Integer, ForeignKey('mail.id')),
                          Column('conversation.id', Integer, ForeignKey('conversation.id'))
                          )


class Conversation(Base):
    __tablename__ = 'conversation'
    id = Column(Integer, primary_key=True)
    title = Column(String(250))
    mails = relationship("Mail", secondary=mail_to_conversation_table)
    participants = relationship("Person", secondary=person_to_conversation_table)


class Person(Base):
    __tablename__ = 'person'
    id = Column(Integer, primary_key=True)
    name = Column(String(250))
    domain = Column(String(250))


class Mail(Base):
    __tablename__ = 'mail'
    subject = Column(String(250))
    msgid = Column("id", String(250), primary_key=True)


session = None


def start_db():
    engine = create_engine('sqlite:///mailman.db')
    Base.metadata.create_all(engine)

    Base.metadata.bind = engine
    DBSession = sessionmaker(bind=engine)

    session = DBSession()
    return session

# Irt a Person in the person table
"""
first_person = Person(name='first person')
session.add(first_person)
session.commit()
second_person = Person(name='second person')
session.add(second_person)
session.commit()

# Insert an Address in the address table
new_address = Conversation(title="A")
new_address.participants.append(first_person)
new_address.participants.append(second_person)
session.add(new_address)
session.commit()
"""