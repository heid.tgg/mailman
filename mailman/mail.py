import json
from imapclient import IMAPClient
import pandas as pd
from . import entities


class MailConnection(object):
    def __init__(self, credential_file):
        credentials = json.load(open(credential_file, "r"))
        self.server = IMAPClient(credentials["server"], use_uid=True)
        self.server.login(credentials["user"], credentials["pwd"])


def read_folder_to_array(connection, folder, columns):
    all = []
    select_info = connection.server.select_folder(folder)
    messages = connection.server.search()
    for (msgid, data) in connection.server.fetch(messages, ['ENVELOPE', 'BODY']).items():
        envelope = data[b'ENVELOPE']
        entry = pd.DataFrame([[envelope.message_id.decode(),
                               envelope.subject.decode() if envelope.subject is not None else "",
                               str(participants(envelope)),
                               envelope.date,
                               envelope.date,
                               envelope.in_reply_to.decode() if envelope.in_reply_to is not None else ""]],
                             columns=columns)
        all.append(entry)
    return all

def create_folder(folder):



def update_folder_to_db(connection, folder, db):
    current_ids = []
    for f in folder:
        select_info = connection.server.select_folder(f)
        messages = connection.server.search()
        current_ids += [data[b'ENVELOPE'].message_id.decode() for (msgid, data) in connection.server.fetch(messages, ['ENVELOPE']).items()]

    for mail in db.query(entities.Mail):
        if mail.msgid not in current_ids:
            db.delete(mail)
            db.commit()


def read_folder_to_db(connection, folder, db):
    all = []
    select_info = connection.server.select_folder(folder)
    messages = connection.server.search()
    for (msgid, data) in connection.server.fetch(messages, ['ENVELOPE', 'BODY']).items():
        envelope = data[b'ENVELOPE']
        if not db.query(entities.Mail).filter(entities.Mail.msgid == envelope.message_id.decode()).count():
            mail = entities.Mail()
            mail.msgid = envelope.message_id.decode()
            mail.subject = envelope.subject.decode() if envelope.subject is not None else ""
            db.add(mail)

    db.commit()


def participants(envelope):
    all = []
    try:
        for address in envelope.from_:
            all.append(address.mailbox.decode())
    except (TypeError, AttributeError):
        pass
    try:
        for address in envelope.to:
            all.append(address.mailbox.decode())
    except (TypeError, AttributeError):
        pass
    if envelope.cc is not None:
        for address in envelope.cc:
            all.append(address.mailbox.decode())

    return all
